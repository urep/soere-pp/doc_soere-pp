# Données eddycovariance

<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Objectif et contenu</h4>
Décrire le cycle de vie général du type de données eddycovariance
</div>

Le type de données `eddycovariance` correspond à l'ensemble des données collectées sur les stations eddycovariance du dispositif.

## Cycle de vie {-}

1. Les données eddycovariance brutes de l'année en cours sont stockées dans le NAS `urep-stockage`
2. 


```{r cdvEddycovariance, echo=FALSE,out.width='100%',fig.show='hold',fig.cap="Cycle de vie du type de données eddycovariance"}
knitr::include_graphics('figures/cdv_eddycovariance.png')
```

## Format d'échange des fichiers bruts

Pour des raisons d'homogénéité, de gain d'espace disque et de facilité d'emploi avec le logiciel EddyPro, les fichiers bruts en sortie des stations `eddycovariance` se présente sous la forme d'un fichier compressé au format `ghg` comportant un fichier de données et un fichier de métadonnées : 

- aaaa-mm-jj.data : Contenu des données
- aaaa-mm-jj.metadata : Données descriptives

Le nom du fichier est construit selon la logique suivante :

`aaaa-mm-jjT[code interne qui s'incrémente +3000]_AIU-1729.ghg`

Ce type de format est généré automatiquement sur les stations LI-7550 et LI-7200 présent sur le site de Laqueuille.

<div class="alert alert-info" role="alert">
  <h4 class="alert-heading">Exemple</h4>
Les données brutes du 12 avril 2018 provenant d'un logger LICOR : 2018-04-12T140000_AIU-1729.ghg
</div>

## Stockage sur le NAS



### Caractéristique de l'envoi des fichiers

Pour la tourbière de la Guette, les Fichiers bruts en sortie de la station eddy-covariance sont intégrés et stockés automatiquement sur le serveur FTP `SRV-SO` via un protocole GPRS : 

- Fréquence d'envoi : Toutes les 30 minutes
- Nombre de fichier par jour: 48
- Taille : environ 144 mo























## Stockage des données sur le NAS

Le NAS `urep-stockage` centralise l'ensemble des données eddycovariance tout au long de leur cycle de vie. Un plan de classement des dossiers du NAS `urep-stockage` est mis en oeuvre pour assurer le suivi des traitements réalisés sur ces données.

<div class="alert alert-warning" role="alert">
<h4 class="alert-heading"><i class="fa fa-warning"></i> À retenir </h4>
- Ne jamais modifier les fichiers bruts,
- Ni espace, ni de caractères spéciaux (sauf underscore "_") : **% $ ! & / \: ; « » % & # @...**
- Tout ce qui est présent dans le répertoire **tmp** peut être supprimé à n'importe quel moment
</div> 

### Plan de classement

Le plan de classement s'appuit sur la logique d'organisation décrite dans la section : [Logique d'organisation]. Pour les données eddycovariance, voici un exemple pour les trois stations de Laqueuille :

```
├── ec_ext
│   └── eddycovariance
│       ├── 0_rawdata
│       │   ├── L1_LI7200
│       │   └── L6_LI7500
│       ├── 1_code
│       ├── 2_pipeline
│       ├── 3_output
│       │   ├── acbb-pp
│       │   ├── fluxNet
│       │   └── ICOS
│       └── tmp
```

