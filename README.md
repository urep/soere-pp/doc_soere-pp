# Documentation SI SOERE ACBB-PP

Document décrivant l'organisation des données collectées sur l'IR SOERE ACBB-PP. Test, mon nouveau commit

```r
install.package("bookdown")
```

## Compilation du document

- Dans R :

```
bookdown::render_book(input="index.rmd","all")
```

- Dans un terminal

```
make all
```

## GitLab Pages

Le fichier `.gitlab-ci.yml` permet de déployer le bookdown avec le système CI/CD (Intégration Continu/Déploiement cContinu) de GitLab sur cette page : [https://urep.pages.mia.inra.fr/soere-pp/doc_soere-pp](https://urep.pages.mia.inra.fr/soere-pp/doc_soere-pp)