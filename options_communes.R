#' @title importparametres
#'
#' @description Fonction pour charger les libraries, et les variables du projet
#'
#' @param repmaster Chemin vers la racine de la copie du dépôt Git en local (XX/XX/)
#'
#' @author Jean-Baptiste Paroissien
#' @keywords 
#' @seealso 
#' @export
#' @examples
#' ## Ne fonctionne pas 
# importparametres(repmaster="/media/sf_GIS_ED/Dev/Scripts/master/",dsn="PG:dbname='sol_elevage' host='localhost' port='5432' user='jb'")

importparametres <- function(repmaster)
{
  assign("reptables",paste(repmaster,"Tables/",sep=""),.GlobalEnv)
  assign("repfigures",paste(repmaster,"Figures/",sep=""),.GlobalEnv)
}


importparametres(repmaster=getwd()) # Ne pas modifier, lancement de la fonction pour générer l'ensemble des paramètres

listPackages <- c('dplyr', 'kableExtra', 'htmltools', 'flextable', 'data.table','ggplot2','ggthemes','scales','stringr')
sapply(listPackages,library,character.only=TRUE)

#lapply(listPackages, function(pkg) {
 # if (system.file(package = pkg) == '') install.packages(pkg)
#})

options(
  htmltools.dir.version = FALSE, formatR.indent = 2,
  width = 55, digits = 4, warnPartialMatchAttr = FALSE, warnPartialMatchDollar = FALSE
)

linkAlertCSS <- function(text,url){
	HTML(paste0("<a href=",url," style=\"color: white;text-decoration:underline;font-weight:bold\">",text,"</a>"))
}